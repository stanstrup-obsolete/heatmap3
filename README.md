heatmap.3
========

Replacement function for gplots' heatmap.2 function. This function calculates distance after scaling in contrast to heatmap.2.
The changes to this function compared to heatmap.2 was made by Thomas W. Leja.


Original comment by Thomas W. Leja:

```R
## original R code $Id: heatmap.2.R 1724 2013-10-11 20:19:22Z warnes $
## updated by Thomas W. Leja: heatmap.3 2013-12-04
## Updates include: 
## 1. z-score data transformation prior to the clustering: scale=c("none","row","column")
## 2. option to reassign the extremes within the scaled data: zlim=c(-3,3)
## 3. option to switch off dendrogram reordering: reorder=FALSE
```
